Translating words from English to other languages

languages = {
    'en': 'English',
    'hello': 'Hello',
    'goodbye': 'Goodbye',
    'bye': 'Goodbye',

    'fr': 'Français',
    'hello_fr': 'Bonjour',
    'goodbye_fr': 'Au revoir',
    'bye_fr': 'Au revoir',

}

def translate(word):
    if word in languages:
        return languages[word]
    else:
        return word